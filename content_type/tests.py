from django.test import TestCase
from faker import Faker

from .models import Automobile1, Automobile2


class Performance10TestCase(TestCase):
    size = 10

    def setUp(self):
        faker = Faker()

        for i in range(self.size):
            automobile = Automobile1.objects.create(
                car1_int_field=faker.pyint(),
                car1_char_field=faker.text(max_nb_chars=255)
            )
            automobile.cars.create(
                brand='Brand A',
                model='Model #%s' % i,
                year=faker.pyint()
            )
            automobile = Automobile2.objects.create(
                car2_int_field=faker.pyint(),
                car2_char_field=faker.text(max_nb_chars=255)
            )
            automobile.cars.create(
                brand='Brand A',
                model='Model #%s' % i,
                year=faker.pyint()
            )

    def test_child_count(self):
        self.assertEqual(2 * self.size, Automobile1.objects.count())
        self.assertEqual(2 * self.size, Automobile2.objects.count())

        # def test_total_count(self):
        #     self.assertEqual(4 * self.size, Car.objects.count())
        #
        # def test_child_count_with_filtering(self):
        #     self.assertEqual(self.size, Automobile1.objects.filter(brand='Brand A').count())
        #     self.assertEqual(self.size, Automobile1.objects.filter(brand='Brand B').count())
        #     self.assertEqual(self.size, Automobile2.objects.filter(brand='Brand A').count())
        #     self.assertEqual(self.size, Automobile2.objects.filter(brand='Brand B').count())
        #
        # def test_total_count_with_filtering(self):
        #     self.assertEqual(2 * self.size, Car.objects.filter(brand='Brand A').count())
        #     self.assertEqual(2 * self.size, Car.objects.filter(brand='Brand B').count())
        #
        # def test_simple_child_iterating(self):
        #     list(Automobile1.objects.all())
        #     list(Automobile2.objects.all())
        #
        # def test_itertools_child_iterating(self):
        #     list(itertools.chain(
        #         Automobile1.objects.all(),
        #         Automobile2.objects.all()
        #     ))
        #
        # def test_total_simple_iterating(self):
        #     list(Car.objects.all())
        #
        # def test_update_query(self):
        #     Automobile1.objects.filter(brand='Brand A').update(brand='Brand AA')
        #     Automobile1.objects.filter(brand='Brand B').update(brand='Brand BB')
        #     Automobile2.objects.filter(brand='Brand A').update(brand='Brand AA')
        #     Automobile2.objects.filter(brand='Brand B').update(brand='Brand BB')
        #
        # def test_total_update_query(self):
        #     Car.objects.filter(brand='Brand A').update(brand='Brand AA')
        #     Car.objects.filter(brand='Brand B').update(brand='Brand BB')


class Performance100TestCase(Performance10TestCase):
    size = 100


class Performance1000TestCase(Performance10TestCase):
    size = 1000
