import factory
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models


class Car(models.Model):
    brand = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    year = models.IntegerField()

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey()

    class Meta:
        unique_together = (
            ('brand', 'model', 'year'),
        )

    def __unicode__(self):
        return '%s, %s, %s' % (self.brand, self.model, self.year)


class Automobile1(models.Model):
    car1_int_field = models.IntegerField()
    car1_char_field = models.CharField(max_length=255)
    cars = generic.GenericRelation(Car)


class Automobile2(models.Model):
    car2_int_field = models.IntegerField()
    car2_char_field = models.CharField(max_length=255)
    cars = generic.GenericRelation(Car)


class Automobile1Factory(factory.django.DjangoModelFactory):
    class Meta:
        model = Automobile1

    car1_int_field = factory.Faker('pyint')
    car1_char_field = factory.Faker('text', max_nb_chars=255)


class CarFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Car

    brand = factory.Faker('company')
    model = factory.Sequence(lambda n: 'Model #%s' % n)
    year = factory.Faker('year')
    content_object = factory.SubFactory(Automobile1Factory)


class Automobile2Factory(factory.django.DjangoModelFactory):
    class Meta:
        model = Automobile2

    car2_int_field = factory.Faker('pyint')
    car2_char_field = factory.Faker('text', max_nb_chars=255)
