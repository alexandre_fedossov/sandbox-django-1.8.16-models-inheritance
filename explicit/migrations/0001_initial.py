# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('brand', models.CharField(max_length=255)),
                ('model', models.CharField(max_length=255)),
                ('year', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Automobile1',
            fields=[
                ('car_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='explicit.Car')),
                ('car1_int_field', models.IntegerField()),
                ('car1_char_field', models.CharField(max_length=255)),
            ],
            bases=('explicit.car',),
        ),
        migrations.CreateModel(
            name='Automobile2',
            fields=[
                ('car_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='explicit.Car')),
                ('car2_int_field', models.IntegerField()),
                ('car2_char_field', models.CharField(max_length=255)),
            ],
            bases=('explicit.car',),
        ),
        migrations.AlterUniqueTogether(
            name='car',
            unique_together=set([('brand', 'model', 'year')]),
        ),
    ]
