from django.db import models


class Parent(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)


class RelatedChild(models.Model):
    parent = models.ForeignKey('nested_batch.Parent')
    created_at = models.DateTimeField(auto_now_add=True)
