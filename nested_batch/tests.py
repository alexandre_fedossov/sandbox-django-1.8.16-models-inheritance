from django.test import TestCase

from .models import Parent, RelatedChild


class BulkCreationWithNestedObjectsTestCase(TestCase):
    def test_creation_1(self):
        parents, childs = [], []

        for i in xrange(10):
            parent = Parent()
            parents.append(parent)

            for j in xrange(3):
                child = RelatedChild(parent=parent)
                childs.append(child)

        Parent.objects.bulk_create(parents)
        RelatedChild.objects.bulk_create(childs)

        self.assertEqual(len(parents), Parent.objects.count())
        self.assertEqual(len(childs), RelatedChild.objects.count())

    def test_creation_2(self):
        parents, childs = [], []

        for i in xrange(10):
            parent = Parent()
            parents.append(parent)

        Parent.objects.bulk_create(parents)

        for parent in Parent.objects.all():
            for j in xrange(3):
                child = RelatedChild(parent=parent)
                childs.append(child)

        RelatedChild.objects.bulk_create(childs)

        self.assertEqual(len(parents), Parent.objects.count())
        self.assertEqual(len(childs), RelatedChild.objects.count())
