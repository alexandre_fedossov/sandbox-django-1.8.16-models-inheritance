import itertools

from django.test import TestCase

from .models import Automobile1Factory, Automobile2Factory, Automobile1, Automobile2


class Performance10TestCase(TestCase):
    size = 10

    def setUp(self):
        Automobile1Factory.create_batch(self.size, brand='Brand A')
        Automobile1Factory.create_batch(self.size, brand='Brand B')
        Automobile2Factory.create_batch(self.size, brand='Brand A')
        Automobile2Factory.create_batch(self.size, brand='Brand B')

    def test_child_count(self):
        self.assertEqual(2 * self.size, Automobile1.objects.count())
        self.assertEqual(2 * self.size, Automobile2.objects.count())

    def test_child_count_with_filtering(self):
        self.assertEqual(self.size, Automobile1.objects.filter(brand='Brand A').count())
        self.assertEqual(self.size, Automobile1.objects.filter(brand='Brand B').count())
        self.assertEqual(self.size, Automobile2.objects.filter(brand='Brand A').count())
        self.assertEqual(self.size, Automobile2.objects.filter(brand='Brand B').count())

    def test_simple_child_iterating(self):
        list(Automobile1.objects.all())
        list(Automobile2.objects.all())

    def test_simple_child_iterating(self):
        list(Automobile1.objects.all())
        list(Automobile2.objects.all())

    def test_itertools_child_iterating(self):
        list(itertools.chain(
            Automobile1.objects.all(),
            Automobile2.objects.all()
        ))

    def test_update_query(self):
        Automobile1.objects.filter(brand='Brand A').update(brand='Brand AA')
        Automobile1.objects.filter(brand='Brand B').update(brand='Brand BB')
        Automobile2.objects.filter(brand='Brand A').update(brand='Brand AA')
        Automobile2.objects.filter(brand='Brand B').update(brand='Brand BB')


class Performance100TestCase(Performance10TestCase):
    size = 100