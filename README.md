# Model inheritance in Django 1.8.16

## Tested variants

There is 3 know ways of implementation:

* Abstract inheritance
* Explicit inheritance
* Using Content Types 

Parent model - Car
First child model - Automobile1 (based on Car) 
Second child model - Automobile2 (based on Car) 

## Instance creation

Implemented via batch creation [Factory Boy](http://factoryboy.readthedocs.io/en/latest/index.html) 
and [Faker](https://faker.readthedocs.io/en/latest/index.html) Python packages.


### Notes for Abstract inheritance

Unable to create Car instances because that model is `abstract` and doesn't have any `Django Model Manager`:

```
AttributeError: type object 'Car' has no attribute '_default_manager'
```

### Notes for Explicit inheritance

### Notes for Content Types

* A good post about it at [stackoverflow](http://stackoverflow.com/questions/20895429/how-exactly-do-django-content-types-work).
* Another post [(in Russian)](https://aliev.me/prosto-o-django-content-types-framework/)
* Unable to create Factory for this type of models.


